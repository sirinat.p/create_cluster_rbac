/// Students

// create service accounts

for i in {1..12}; 
do

k create ns t$i;
k create sa t$i -n t$i;
k create role t$i --resource=*.* --verb=* -n t$i;
k create rolebinding t$i --role t$i --serviceaccount=t$i:t$i -n t$i;

done

// create service account kube configs

for i in {1..12}; 
do

./create_kube_config.sh t$i t$i;

done

// create ssh keypairs 

for i in {1..12}; do
ssh-keygen -t rsa -m pem -q -N "" -f sshkey/t${i}_rsa;
done

// create ubuntu accounts

gcloud compute ssh kd-ubuntu-set --zone us-west1-a;
for i in {1..12}; 
do
sudo useradd -s /bin/bash t$i -m -d /home/t$i;
sudo mkdir -p /home/t$i/.ssh;
sudo mkdir -p /home/t$i/.kube;
sudo chown -R t$i:t$i /home/t$i/;
done

// delete ubuntu accounts
for i in {1..12}; 
do
sudo userdel t$i
sudo rm -rf /home/t$i;
done

// copy kube configs and ssh key pairs

for ia in {1..12}; 
do
gcloud compute scp sshkey/t${ia}_rsa.pub root@kd-ubuntu-set:/home/t$ia/.ssh/authorized_keys --zone us-west1-a;
gcloud compute scp kubeconfig/t$ia root@kd-ubuntu-set:/home/t$ia/.kube/config --zone us-west1-a;
done

// grant user permissions to the ubuntu accounts

gcloud compute ssh kd-ubuntu-set --zone us-west1-a;
for i in {1..12}; 
do
sudo chown -R t$i:t$i /home/t$i/;
done

/// Admin

// create service account

k create ns staffkube;
k create sa staffkube -n staffkube;
k create clusterrolebinding staffkube --clusterrole cluster-admin --serviceaccount=staffkube:staffkube;

// create service account kube config

./create_kube_config.sh staffkube staffkube;

// create ssh keypairs 

ssh-keygen -t rsa -m pem -q -N "" -f sshkey/staffkube_rsa;

// create ubuntu account

gcloud compute ssh kd-ubuntu-set --zone us-west1-a;
sudo useradd staffkube -s /bin/bash -m -d /home/staffkube;
sudo mkdir -p /home/staffkube/.ssh;
sudo mkdir -p /home/staffkube/.kube;
sudo chown -R staffkube:staffkube /home/staffkube/;

// delete ubuntu account
sudo userdel staffkube
sudo rm -rf /home/staffkube

// copy kube configs and ssh key pairs

gcloud compute scp sshkey/staffkube_rsa.pub root@kd-ubuntu-set:/home/staffkube/.ssh/authorized_keys --zone us-west1-a;
gcloud compute scp kubeconfig/staffkube root@kd-ubuntu-set:/home/staffkube/.kube/config --zone us-west1-a;

// grant user permissions to the ubuntu account

gcloud compute ssh kd-ubuntu-set --zone us-west1-a;
sudo chown -R staffkube:staffkube /home/staffkube/;